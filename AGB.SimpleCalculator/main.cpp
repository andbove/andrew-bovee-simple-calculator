#include <iostream>	

using namespace std;

float Add(float num1, float num2) 
{
	return num1 + num2;
}
float Subtract(float num1, float num2)
{
	return num1 - num2;
}
float Multiply(float num1, float num2)
{
	return num1 * num2;
}
bool Divide(float num1, float num2, float &answer)
{
	if (num2 == 0)
	{
		return false;
	}
	else
	{
		answer = num1 / num2;
		return true;
	}

	//return num1 / num2;//Watch video gives solution need &answer
}


int main()
{
	float num1, num2, sum, answer;
	char oper;

	cout << "Basic Calculator: \n";
	cout << "Enter the first number in the equation: "; cin >> num1;
	cout << "Enter a +, -, *, /, or ^: "; cin >> oper;
	cout << "Enter the second number in the equation: \n"; cin >> num2;
	

		if (oper == '+')
		{
			sum = Add(num1, num2);
			cout << num1 << " + " << num2 << " = " << sum;
		}
		else if (oper == '-')
		{
			sum = Subtract(num1, num2);
			cout << num1 << " - " << num2 << " = " << sum;
		}
		else if (oper == '*')
		{
			sum = Multiply(num1, num2);
			cout << num1 << " * " << num2 << " = " << sum;
		}
		else if (oper == '/')
		{
			if (Divide(num1, num2, answer))
			{
				cout << num1 << " / " << num2 << " = " << answer;
			}
			else 
			{
				cout << "Error, can't divide by 0";
			}
		}
		else if (oper == '^')
		{
			int i = 1;
			float power = float(1);
			while (i <= num2)
			{
				power = power * num1;
				i++;
			}
			cout << num1 << " to the " << num2 << " power "  << " is: " << power;
		}
		exit(3);
		return 3;

}